package com.example.vandh.chatapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by vandh on 29-06-2017.
 */

public class SQLite_reference extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "SQLitetoken.db";
    private static final int DATABASE_VERSION = 1;
    public static final String PERSON_TABLE_NAME = "user_tokens";
    public static final String USER_COLUMN_ID = "id";
    public static final String token = "token";
    public static int i=0;

    public SQLite_reference(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + PERSON_TABLE_NAME + "(" +
                USER_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                token + " TEXT)"
        );

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean insertPerson(String tokentemp) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(token, tokentemp);
        db.insert(PERSON_TABLE_NAME, null, contentValues);
        return true;
    }
}
