package com.example.vandh.chatapp;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by vandh on 12-06-2017.
 */
public class convo_details implements Serializable {
    private String receiver;
    private String convoid;
    private String description;
    private Date lastdate;

    public Date getLastdate() {
        return lastdate;
    }

    public void setLastdate(Date lastdate) {
        this.lastdate = lastdate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getConvoid() {
        return convoid;
    }

    public void setConvoid(String convoid) {
        this.convoid = convoid;
    }






}
