package com.example.vandh.chatapp;

import java.util.Date;

/**
 * Created by vandh on 04-06-2017.
 */
public class message_details {
    private String text;
    private String sender;
    private Date curdate;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Date getCurdate() {
        return curdate;
    }

    public void setCurdate(Date curdate) {
        this.curdate = curdate;
    }
}
