package com.example.vandh.chatapp;

import android.app.Application;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import com.firebase.client.Firebase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    private ArrayList<message_details> messagelist;
    private customAdaptor adapter;
    private String receiver;
    private ListView list;
    private Date cdate=new Date();
    private MessageDataSource.MessagesListener listener;
    private String convoid;
    private static int flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        final EditText newMessage = (EditText)findViewById(R.id.newmessage);
        Intent i=getIntent();
        receiver =i.getStringExtra("recv");
        flag=0;
        setTitle(receiver);
        if (getSupportActionBar() != null) {//will set backarrow in action bar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        list = (ListView) findViewById(R.id.messageslist);
        list.setItemsCanFocus(true);
        messagelist = new ArrayList<>();
        adapter = new customAdaptor(messagelist);
        list.setAdapter(adapter);
        final SimpleDateFormat sDateFormat = new SimpleDateFormat("HH:mm:ss");

        final ImageButton sendMessage = (ImageButton) findViewById(R.id.sendMessageButton);
        sendMessage.setOnClickListener(this);
        convoid =i.getStringExtra("convoid");
        newMessage.addTextChangedListener(new TextWatcher() {//text change listener
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {//changes the microphone icon to send icon
                sendMessage.setBackgroundResource(R.drawable.sentshape);
                sendMessage.invalidate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        listener = MessageDataSource.addMessagesListener(receiver, this);//activate the listener for firebase child

        //Displays time when a message is clicked
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                messagelist.get(position); // here you will get the clicked item from
                //your imagelist and you can check by getting a title  by using this

                TextView tv=(TextView) view.findViewById(R.id.timez);
                if(flag==0)
                {
                    String title = sDateFormat.format(messagelist.get(position).getCurdate());
                    tv.setText(title);
                    tv.setVisibility(view.VISIBLE);
                    flag=1;
                }
                else
                {
                    tv.setVisibility(view.GONE);
                    flag=0;
                }


                System.out.println(R.id.timez);
            }
        });

    }

    // Triggers when message send button is clicked in content_main.xml
    @Override
    public void onClick(View v){
        EditText newMessageView = (EditText)findViewById(R.id.newmessage);
        String newMessage = newMessageView.getText().toString();
        System.out.println(newMessage);
        newMessageView.setText("");
        message_details msg = new message_details();//stores message
        msg.setCurdate(new Date());
        msg.setText(newMessage);
        msg.setSender("Vandana");
        MessageDataSource.saveMessage(msg, convoid,receiver);//call to save message in MessageDataSource.class
        v.setBackgroundResource(R.drawable.circle_shape);
        v.invalidate();
    }

    // returns to previous activity when back arrow is clicked in action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    //To notify the adapter when arraylist is updated and it is called from MessageDataSource class
    public void onMessageAdded(message_details ms)
    {
        messagelist.add(ms);
        adapter.notifyDataSetChanged();
    }


    //To destroy the childevent listener
    @Override
    protected void onDestroy() {
        System.out.println("main on destroy");
        super.onDestroy();
        MessageDataSource.stop(listener);
    }

    private class customAdaptor extends ArrayAdapter<message_details>//custom adapter for listview
    {
        public customAdaptor(ArrayList<message_details> msg)
        {
            super(MainActivity.this, R.layout.text2, R.id.messagesent, msg);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = super.getView(position, convertView, parent);
            //updating the textview and imageview in text2.xml file for diaplaying chat details
            message_details message = getItem(position);
            TextView nameView = (TextView)convertView.findViewById(R.id.messagesent);
            nameView.setText(message.getText());
            TextView timeView = (TextView)convertView.findViewById(R.id.timez);
            ImageView iview=(ImageView)convertView.findViewById(R.id.user_img);
            RelativeLayout lout=(RelativeLayout) convertView.findViewById(R.id.relative);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);

            //LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)nameView.getLayoutParams();
            //RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams)lout.getLayoutParams();
            int sdk = Build.VERSION.SDK_INT;
            if (message.getSender().equals("Vandana")){

                System.out.println("adapter call1");
                if (sdk >= Build.VERSION_CODES.JELLY_BEAN) {//to change the Bcakground and gravity for sender and receiver
                    nameView.setBackground(getDrawable(R.drawable.bubble_right_green));

                } else{
                    nameView.setBackgroundDrawable(getDrawable(R.drawable.bubble_right_green));
                }
                iview.setVisibility(View.GONE);
                lout.setGravity(Gravity.RIGHT);

            }else{
                if (sdk >= Build.VERSION_CODES.JELLY_BEAN) {
                    nameView.setBackground(getDrawable(R.drawable.bubble_left_gray));
                } else{
                    nameView.setBackgroundDrawable(getDrawable(R.drawable.bubble_left_gray));
                }
                iview.setVisibility(View.VISIBLE);
                lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                //timeView.setLayoutParams(lp);
                lout.setGravity(Gravity.LEFT);

            }


            return convertView;
        }


    }



}

