package com.example.vandh.chatapp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by vandh on 12-06-2017.
 */
public class startActivity extends ActionBarActivity {
    private ListView list;
    private String des;
    private customAdapter adapter;
    public  ArrayList<convo_details> alist = null;
    private static SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    private static final Firebase sRef = new Firebase("https://chatapp-cff3c.firebaseio.com/");
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startlayout);
        list = (ListView) findViewById(R.id.list);
        alist = new ArrayList<>();
        adapter = new customAdapter(alist);
        list.setAdapter(adapter);


        SQLite_reference sqLite_obj=new SQLite_reference(this);
        sqLite_obj.insertPerson(FirebaseInstanceId.getInstance().getToken());
        //FirebaseDatabase database = FirebaseDatabase.getInstance();
        //DatabaseReference myRef = database.getReference();
        sRef.child("mess").setValue("hi");
        sRef.child("token").setValue(FirebaseInstanceId.getInstance().getToken());
        Log.d("firebasetoken", "New Token: " + FirebaseInstanceId.getInstance().getToken());
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                convo_details con = alist.get(position); // here you will get the clicked item from
                //shifts to mainactivity to display chat
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("convoid", con.getConvoid());
                i.putExtra("recv", con.getReceiver());
                startActivity(i);

            }
        });
        sRef.keepSynced(true);
        sRef.child("vandana").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {//to check any new conversation start
                final String recv = dataSnapshot.getKey();
                final convo_details con = new convo_details();
                sRef.child("vandana").child(recv).limitToLast(1).addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {//to chck any new conversation addition to particular receiver
                        HashMap<String, String> msg = (HashMap) dataSnapshot.getValue();
                        des = msg.get("text");
                        con.setDescription(des);
                        try {
                            con.setLastdate(sDateFormat.parse(dataSnapshot.getKey()));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        adapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });

                con.setReceiver(recv);
                con.setConvoid("Vandana-" + recv);
                System.out.println("ello 1- " + con.getDescription() + des);
                alist.add(con);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


    }



    private class customAdapter extends ArrayAdapter<convo_details> {
        public customAdapter(ArrayList<convo_details> msg) {
            super(startActivity.this, R.layout.text3, R.id.convo, msg);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            long days=0;
            convertView = super.getView(position, convertView, parent);
            SimpleDateFormat s1DateFormat = new SimpleDateFormat("HH:mm");
            convo_details message = getItem(position);
            TextView nameView = (TextView) convertView.findViewById(R.id.convo);
            nameView.setText(message.getReceiver());
            TextView tView = (TextView) convertView.findViewById(R.id.desc2);
            TextView timeview=(TextView) convertView.findViewById(R.id.timedisplay);
            tView.setText(message.getDescription());
            timeview.setText(s1DateFormat.format(new Date()));
            //SimpleDateFormat myFormat = new SimpleDateFormat("yyyyMMdd");

            /*try {
                Date date1 = myFormat.parse(myFormat.format(message.getLastdate()));
                Date date2 = myFormat.parse(myFormat.format(new Date()));
                long diff = date2.getTime() - date1.getTime();
                days=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            long n;
            if(days==0) {
                timeview.setText(s1DateFormat.format(message.getLastdate()));
            }
            else if((days/7)==0)
            {
                timeview.setText(String.valueOf(days)+"d");
            }
            else if((n=days/7)<5)
            {
                timeview.setText(String .valueOf(n)+"w");
            }
            else if((n=days/29)>12)
            {
                timeview.setText(String.valueOf(n/12));
            }*/


            return convertView;
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {// To create plus icon in action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_plus, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {//to create a new message when + icon is clicked
        switch (item.getItemId()) {
            // action with ID action_settings was selected
            case R.id.menu_plus:
                // this is where you put your own code to do what you want.
                Toast.makeText(this, "Menu Item 1 selected", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), listing_chats.class);
                Bundle args = new Bundle();
                args.putSerializable("chatlist", (Serializable) alist);
                i.putExtra("BUNDLE", args);
                startActivity(i);
                break;
            default:
                break;
        }

        return true;
    }


}
