package com.example.vandh.chatapp;

        import android.util.Log;

        import com.firebase.client.ChildEventListener;
        import com.firebase.client.DataSnapshot;
        import com.firebase.client.Firebase;
        import com.firebase.client.FirebaseError;

        import java.text.SimpleDateFormat;
        import java.util.Date;
        import java.util.HashMap;

/**
 * Created by Ashu on 24/11/15.
 */
public class MessageDataSource {
    private static final Firebase sRef = new Firebase("https://chatapp-cff3c.firebaseio.com/");
    private static SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    private static final String TAG = "MessageDataSource";
    private static final String COLUMN_TEXT = "text";
    private static final String COLUMN_SENDER = "sender";


    public static void saveMessage(message_details message, String convoId,String recv){
        Date date = message.getCurdate();
        String key=sDateFormat.format(date);
        HashMap<String, String> msg = new HashMap<>();
        msg.put(COLUMN_TEXT, message.getText());
        msg.put(COLUMN_SENDER, "Vandana");
        sRef.keepSynced(true);
        sRef.child("vandana").child(recv).child(key).setValue(msg);
    }


    public static MessagesListener addMessagesListener(String receiver, final MainActivity callbacks){
        MessagesListener listener = new MessagesListener(callbacks);
        sRef.child("vandana").child(receiver).addChildEventListener(listener);
        return listener;

    }

    public static void stop(MessagesListener listener){
        sRef.removeEventListener(listener);
    }

    public static class MessagesListener implements ChildEventListener {
        private MainActivity callbacks;
        MessagesListener(MainActivity callbacks){
            this.callbacks = callbacks;
        }
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            HashMap<String,String> msg = (HashMap)dataSnapshot.getValue();
            message_details message = new message_details();
            message.setSender(msg.get(COLUMN_SENDER));
            message.setText(msg.get(COLUMN_TEXT));
            try {
                message.setCurdate(sDateFormat.parse(dataSnapshot.getKey()));
            }catch (Exception e){
                Log.d(TAG, "Couldn't parse date" + e);
            }
            if(callbacks != null){
                callbacks.onMessageAdded(message);
            }

        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {


        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(FirebaseError firebaseError) {

        }
    }



}
