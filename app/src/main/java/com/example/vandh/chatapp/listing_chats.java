package com.example.vandh.chatapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by vandh on 13-06-2017.
 */
public class listing_chats extends ActionBarActivity {


    private ListView list;
    private customAdapter adapter;
    private EditText adduser;
    private Button addbutton;
    public  ArrayList<convo_details> alist;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_chats);
        list=(ListView) findViewById(R.id.conlist);
        addbutton=(Button)findViewById(R.id.add);
        adduser=(EditText)findViewById(R.id.type_username);
        System.out.println("called listing chats");
        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        alist = (ArrayList<convo_details>) args.getSerializable("chatlist");
        System.out.println("read the array list");
        adapter = new customAdapter(alist);
        list.setAdapter(adapter);
        setTitle("New message");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        addbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String recv=adduser.getText().toString();
                //return the username
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("convoid", "Vandana-"+recv);
                i.putExtra("recv", recv);
                startActivity(i);
                //---close the activity---
                finish();

            }
        });
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                convo_details con = alist.get(position); // here you will get the clicked item from
                //your imagelist and you can check by getting a title  by using this
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("convoid", con.getConvoid());
                i.putExtra("recv", con.getReceiver());
                startActivity(i);

            }
        });




    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class customAdapter extends ArrayAdapter<convo_details>
    {
        public customAdapter(ArrayList<convo_details> msg)
        {
            super(listing_chats.this, R.layout.text, R.id.convo, msg);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = super.getView(position, convertView, parent);
            convo_details message = getItem(position);
            System.out.println("adapter call");
            TextView nameView = (TextView)convertView.findViewById(R.id.convo);
            nameView.setText(message.getReceiver());
            //nameView.setTag(position);
            //LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)nameView.getLayoutParams();



            return convertView;
        }


    }




}
